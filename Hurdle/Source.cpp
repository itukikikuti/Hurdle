#include "XLibrary11.hpp"
using namespace XLibrary11;

enum Mode
{
	Title,
	Game
};

int Main()
{
    Camera camera;

	Mode mode = Mode::Title;

    Sprite backgroundSprite(L"Background.png");
    Sprite playerSprite(L"Player.png");
    Sprite hurdleSprite(L"Hurdle.png");
	Text titleText(L"�n�[�h����", 100.0f);

	titleText.position.y = 100.0f;

    float fallingSpeed = 0.0f;
    bool jumpFlag = false;

    while (Refresh())
    {
		camera.Update();

		switch (mode)
		{
		case Mode::Title:

			if (Input::GetKeyDown(VK_SPACE))
			{
				playerSprite.position.x = -100.0f;
				hurdleSprite.position.x = -320.0f;
				hurdleSprite.position.y = -80.0f;

				mode = Mode::Game;
			}

			titleText.Draw();

			break;
		case Mode::Game:

			hurdleSprite.position.x -= 10.0f;

			if (hurdleSprite.position.x < -320.0f)
			{
				if (Random::GetValue() < 0.1f)
				{
					hurdleSprite.position.x = 320.0f;
				}
			}

			fallingSpeed -= 1.0f;

			if (Input::GetKeyDown(VK_SPACE))
			{
				if (jumpFlag == true)
				{
					fallingSpeed = 10.0f;
					jumpFlag = false;
				}
			}

			playerSprite.position.y += fallingSpeed;
			if (playerSprite.position.y < -80.0f)
			{
				playerSprite.position.y = -80.0f;
				jumpFlag = true;
			}

			playerSprite.angles.z = Random::Range(-30.0f, 30.0f);

			if (hurdleSprite.position.x - 16.0f < playerSprite.position.x &&
				hurdleSprite.position.x + 16.0f > playerSprite.position.x &&
				hurdleSprite.position.y - 16.0f < playerSprite.position.y &&
				hurdleSprite.position.y + 16.0f > playerSprite.position.y)
			{
				mode = Mode::Title;
			}

			break;
		}

        backgroundSprite.Draw();
		playerSprite.Draw();
        hurdleSprite.Draw();
	}

    return 0;
}
